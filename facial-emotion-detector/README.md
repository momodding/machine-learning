Image Captioning
Give caption into image

Requirements :
- Python 3
- Keras == 2.1.6
- Numpy
- tqdm
- Tensorflow == 1.5
- Falcon
- Gunicorn

Dataset : Flickr8K

Cara Penggunaan :
- Masuk ke python cli
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Training :
- Buat folder dataset. copy semua data fer2013 ke folder dataset
- Jalankan train_engine.py sampai selesai

Cara Testing API dengan postman:
- isi url dengan localhost:8895/api/predict dan ganti method menjadi POST
- pada tab body pilih form-data dan isikan kolom key dengan image dan type file
- pilih gambar melalui kolom value
