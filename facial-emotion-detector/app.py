import falcon
import json
import os
import io
import falcon_jsonify
from detect_engine import *
from train_engine import *
from keras import backend as K
from falcon_multipart.middleware import MultipartMiddleware


class HelloWorldResource:
    def on_get(self, req, resp, foo):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ("Hello " + foo +
                     ".  Python's Falcon thinks you are awesome!")


class DetekResource:
    _storage_path = './temp'

    def on_post(self, req, resp):
        """Handles POST requests"""
        image = req.get_param('image')

        if image != None:
            raw = image.file.read()

            filename = image.filename

            file_path = os.path.join(self._storage_path, filename)

            temp_file_path = file_path + '~'

            open(temp_file_path, 'wb').write(raw)

            os.rename(temp_file_path, file_path)

            result = predict(file_path)
            K.clear_session()
            resp.status = falcon.HTTP_200
            resp.body = (json.dumps(
                {'status': "success", 'result': result}))
        else:
            resp.status = falcon.HTTP_422
            resp.body = (json.dumps(
                {'status': "error", 'result': 'upload gambar dahulu'}))


class TrainResource:
    def on_post(self, req, resp):
        """Handles POST requests"""
        train_size = req.get_param('train_size')
        batch_size = req.get_param('batch_size')
        epochs = req.get_param('epochs')

        if train_size is not None and batch_size is not None and epochs is not None:
            loss, accuracy = train_two(
                filename="../all/fer2013/fer2013.csv", train_size=0.8)
            K.clear_session()
            resp.status = falcon.HTTP_200
            resp.body = (json.dumps(
                {'status': "success", 'accuracy': accuracy}))
        else:
            resp.status = falcon.HTTP_422
            resp.body = (json.dumps(
                {'status': "error", 'result': 'parameter kurang'}))


app = falcon.API(middleware=[MultipartMiddleware()])
app.req_options.auto_parse_form_urlencoded = True

app.add_route('/hello/{foo}', HelloWorldResource())
app.add_route('/api/predict', DetekResource())
app.add_route('/api/train', TrainResource())
