from keras.preprocessing import image
from keras.models import model_from_json
import numpy as np

emotion_dict = {
    0: 'Angry',
    1: 'Disgust',
    2: 'Fear',
    3: 'Happy',
    4: 'Sad',
    5: 'Surprise',
    6: 'Neutral'
}


def predict(filename):
    img = image.load_img(filename, grayscale=True, target_size=(48, 48))

    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x /= 255

    json_file = open('./model/facial_expression_model_structure_6.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    model.load_weights("./model/facial_expression_model_6_weights.h5")
    print("Loaded model from disk")

    custom = model.predict(x)
    index_max = np.argmax(custom[0])

    predicted = emotion_dict[index_max]
    # print("Emotion :", predicted)
    return predicted


if __name__ == "__main__":
    filename = "jackman.png"
    predicted = predict(filename)
    print("Emotion :", predicted)
