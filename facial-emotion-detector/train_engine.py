import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D
from keras.layers import Dense, Dropout, Flatten
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from keras import backend as K

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.60
config.gpu_options.visible_device_list = "0"
set_session(tf.Session(config=config))


def read_file(filename):
    with open(filename) as f:
        content = f.readlines()
    return content


def train_one(filename, batch_sz=256, n_epochs=20, train_size=0.8):
    num_classes = 7  # angry, disgust, fear, happy, sad, surprise, neutral
    batch_size = batch_sz
    epochs = n_epochs
    # file = "../all/fer2013/fer2013.csv"
    content = read_file(filename)
    lines = np.array(content)

    num_of_instances = lines.size

    X_data, Y_data = [], []

    for i in range(1, num_of_instances):
        try:
            emotion, img, usage = lines[i].split(",")

            if emotion == '2':
                # print(emotion)
                pass
            else:
                val = img.split(" ")

                pixels = np.array(val, 'float32')

                emotion = keras.utils.to_categorical(emotion, num_classes)

                X_data.append(pixels)
                Y_data.append(emotion)
        except:
            print("", end="")

    # TRAIN_SPLIT = 0.7
    X, y = shuffle(X_data, Y_data)
    X_train, X_test = X[:int(train_size * len(X))
                        ], X[int(train_size * len(X)):]
    Y_train, Y_test = y[:int(train_size * len(y))
                        ], y[int(train_size * len(y)):]

    x_train = np.array(X_train, 'float32')
    y_train = np.array(Y_train, 'float32')
    x_test = np.array(X_test, 'float32')
    y_test = np.array(Y_test, 'float32')

    x_train /= 255
    x_test /= 255

    x_train = x_train.reshape(x_train.shape[0], 48, 48, 1)
    x_train = x_train.astype('float32')
    x_test = x_test.reshape(x_test.shape[0], 48, 48, 1)
    x_test = x_test.astype('float32')

    model = Sequential()

    # 1st convolution layer
    model.add(Conv2D(64, (5, 5), activation='relu', input_shape=(48, 48, 1)))
    model.add(MaxPooling2D(pool_size=(5, 5), strides=(2, 2)))

    # 2nd convolution layer
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

    # 3rd convolution layer
    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

    # 4th convolution layer
    model.add(Conv2D(256, (1, 1), activation='relu'))
    model.add(Conv2D(256, (1, 1), activation='relu'))
    model.add(MaxPooling2D(pool_size=(1, 1), strides=(1, 1)))

    model.add(Flatten())

    # fully connected neural networks
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.2))

    model.add(Dense(num_classes, activation='softmax'))

    gen = ImageDataGenerator()
    train_generator = gen.flow(x_train, y_train, batch_size=batch_size)

    model.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adam(), metrics=['accuracy']
                  )

    model.fit_generator(
        train_generator, steps_per_epoch=batch_size, epochs=epochs)

    model_json = model.to_json()
    with open("./model/facial_expression_model_structure_7.json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights("./model/facial_expression_model_7_weights.h5")

    score = model.evaluate(x_test, y_test)
    loss = score[0]
    accuracy = 100*score[1]
    return loss, accuracy


def train_two(filename, batch_sz=256, n_epochs=12, train_size=0.8):
    num_classes = 6  # angry, disgust, fear, happy, sad, surprise, neutral
    batch_size = batch_sz
    epochs = n_epochs
    # file = "../all/fer2013/fer2013.csv"
    content = read_file(filename)
    lines = np.array(content)

    num_of_instances = lines.size
    X_data, Y_data = [], []

    for i in range(1, num_of_instances):
        try:
            emotion, img, usage = lines[i].split(",")

            if emotion == '2':
                # print(emotion)
                pass
            else:
                val = img.split(" ")

                pixels = np.array(val, 'float32')

                emotion = keras.utils.to_categorical(emotion, num_classes)

                X_data.append(pixels)
                Y_data.append(emotion)
        except:
            print("", end="")

    # TRAIN_SPLIT = 0.7
    X, y = shuffle(X_data, Y_data)
    X_train, X_test = X[:int(train_size * len(X))
                        ], X[int(train_size * len(X)):]
    Y_train, Y_test = y[:int(train_size * len(y))
                        ], y[int(train_size * len(y)):]

    x_train = np.array(X_train, 'float32')
    y_train = np.array(Y_train, 'float32')
    x_test = np.array(X_test, 'float32')
    y_test = np.array(Y_test, 'float32')

    x_train /= 255
    x_test /= 255

    x_train = x_train.reshape(x_train.shape[0], 48, 48, 1)
    x_train = x_train.astype('float32')
    x_test = x_test.reshape(x_test.shape[0], 48, 48, 1)
    x_test = x_test.astype('float32')

    model = Sequential()

    # 1st convolution layer
    model.add(Conv2D(64, (5, 5), activation='relu', input_shape=(48, 48, 1)))
    model.add(MaxPooling2D(pool_size=(5, 5), strides=(2, 2)))

    # 2nd convolution layer
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

    # 3rd convolution layer
    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

    # 4th convolution layer
    model.add(Conv2D(256, (1, 1), activation='relu'))
    model.add(Conv2D(256, (1, 1), activation='relu'))
    model.add(MaxPooling2D(pool_size=(1, 1), strides=(1, 1)))

    model.add(Flatten())

    # fully connected neural networks
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.2))

    model.add(Dense(num_classes, activation='softmax'))

    gen = ImageDataGenerator()
    train_generator = gen.flow(x_train, y_train, batch_size=batch_size)

    model.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adam(), metrics=['accuracy']
                  )

    model.fit_generator(
        train_generator, steps_per_epoch=batch_size, epochs=epochs)

    model_json = model.to_json()
    with open("./model/facial_expression_model_structure_6.json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights("./model/facial_expression_model_6_weights.h5")

    score = model.evaluate(x_test, y_test)
    loss = score[0]
    accuracy = 100*score[1]
    return loss, accuracy


if __name__ == "__main__":
    loss, accuracy = train_two(
        filename="./dataset/fer2013.csv", train_size=0.8)
    print('Test loss:', loss)
    print('Test accuracy:', accuracy)
    K.clear_session()
