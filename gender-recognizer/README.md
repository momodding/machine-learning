Semantic Analysis
Detecting similiarity from two sentence or paragraph

Requirements :
- Python 3
- Pandas
- Numpy
- Sklearn
- Falcon
- Gunicorn

Cara Penggunaan :
- Masuk ke python cli
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Training :
- isi url dengan localhost:8887/api/train dan ganti method menjadi POST
- pada tab body pilih raw -> application/json
- {"size":0.8, "engine":"engine 1"}

Note:
- size adalah size training
- engine terdiri dari "engine 1" dan "engine 2"

Cara Testing API dengan postman:
- isi url dengan localhost:8887/api/predict dan ganti method menjadi POST
- pada tab body pilih raw -> application/json
- {"nama":"Jessica", "engine":"engine 1"}

Note:
- engine terdiri dari "engine 1" dan "engine 2"

Akurasi:
- Engine 1 (Multinomial NB) : 83%
- Engine 2 (Decision Tree) : 85%