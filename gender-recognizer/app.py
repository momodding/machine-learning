import falcon
import json
# from core_multiNB import *
# from core_decisiontree import *
import core_multiNB
import core_decisiontree
from checker import *
import falcon_jsonify


class HelloWorldResource:
    def on_get(self, req, resp, foo):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ("Hello " + foo +
                     ".  Python's Falcon thinks you are awesome!")


class DetekResource:
    def on_post(self, req, resp):
        """Handles POST requests"""
        inputan = req.json['nama']
        engine = req.json['engine']
        if cek_text(inputan) <= 1:
            resp.status = falcon.HTTP_500
            resp.body = (json.dumps(
                {'status': "Error", 'message': "Kalimat kurang panjang"}))
        else:
            if engine == 'engine 1':
                nama = stopword(inputan)
                result = core_multiNB.predict(nama)
            else:
                nama = stopword(inputan)
                result = core_decisiontree.predict(nama)
            resp.status = falcon.HTTP_200
            resp.body = (json.dumps(
                {'status': "success", 'prediksi': result}))


class TrainResource:
    def on_post(self, req, resp):
        """Handles POST requests"""
        train_size = req.json['size']
        engine = req.json['engine']
        if engine == 'engine 1':
            accuracy = core_multiNB.train(train_size=train_size)
            print("Akurasi : ", accuracy)
        else:
            accuracy = core_decisiontree.train(train_size=train_size)
            print("Akurasi : ", accuracy)
        resp.status = falcon.HTTP_200
        resp.body = (json.dumps(
            {'status': "success", 'akurasi': accuracy}))


app = falcon.API(middleware=[falcon_jsonify.Middleware(help_messages=True)])
app.req_options.auto_parse_form_urlencoded = True

app.add_route('/hello/{foo}', HelloWorldResource())
app.add_route('/api/predict', DetekResource())
app.add_route('/api/train', TrainResource())
