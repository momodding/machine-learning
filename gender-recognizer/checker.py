from string import *
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords

def cek_text(text):
    """Cek banyak kata dalam teks"""
    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]
    words = [''.join(c for c in s if c not in punctuation) for s in words]

    return len(words)

def stopword(name):
  stopword_file_en = [line.rstrip('\n')
                      for line in open('stopwords/stopwords_en.txt')]
  stopword_file_html = [line.rstrip('\n')
                        for line in open('stopwords/stopwords_html.txt')]
  stopword_file_id = [line.rstrip('\n')
                      for line in open('stopwords/stopwords_id.txt')]
  stopword_file_my = [line.rstrip('\n')
                      for line in open('stopwords/stopwords_my.txt')]
  stopword_file_title = [line.rstrip('\n')
                        for line in open('stopwords/stopwords_title.txt')]
  stopword_forbidden = [line.rstrip('\n')
                        for line in open('gender/dictionary/forbidden_entity.txt')]
  stopword_gelar = [line.rstrip('\n')
                    for line in open('gender/dictionary/gelar.txt')]
  stopword_plus = list()
  stopword_plus = stopword_file_en + stopword_file_html + \
      stopword_file_id + stopword_file_my + \
      stopword_file_title + stopword_forbidden + stopword_gelar
  stopword_plus = set(stopword_plus)
  result = ' '.join([word for word in name.split() if word not in stopword_plus])
  return result
