import pandas as pd
import numpy as np
from sklearn.utils import shuffle
from sklearn.feature_extraction import DictVectorizer
from sklearn.tree import DecisionTreeClassifier
from sklearn import metrics
from sklearn.externals import joblib
from checker import *

vectorizer = DictVectorizer()


def load_data(female, male):
    female_names = pd.read_csv(female, names=['name'])
    male_names = pd.read_csv(male, names=['name'])
    female_names['gender'] = "Female"
    male_names['gender'] = "Male"
    data = female_names.append(male_names, ignore_index=True)
    data = data.drop_duplicates(subset='name', keep=False)
    names = data.as_matrix()[:, 0:]
    return names


def features(name):
    name = name.lower()
    return {
        'first-letter': name[0],  # First letter
        'first2-letters': name[0:2],  # First 2 letters
        'first3-letters': name[0:3],  # First 3 letters
        'last-letter': name[-1],
        'last2-letters': name[-2:],
        'last3-letters': name[-3:],
    }


def data_split(x_data, y_data, TRAIN_SPLIT=0.8):
    X, y = shuffle(x_data, y_data)
    X_train, X_test = X[:int(TRAIN_SPLIT * len(X))
                        ], X[int(TRAIN_SPLIT * len(X)):]
    y_train, y_test = y[:int(TRAIN_SPLIT * len(y))
                        ], y[int(TRAIN_SPLIT * len(y)):]
    return X_train, X_test, y_train, y_test


def measure(target_test, predicted):
    return metrics.accuracy_score(target_test, predicted)


features = np.vectorize(features)


def train(train_size):
    data_female = 'gender/class/female.txt'
    data_male = 'gender/class/male.txt'
    names = load_data(data_female, data_male)
    # features = np.vectorize(features)
    X = features(names[:, 0])
    y = names[:, 1]
    X_train, X_test, y_train, y_test = data_split(X, y, TRAIN_SPLIT=train_size)
    vectorizer.fit(X_train)
    data_train = vectorizer.transform(X_train)
    clf = DecisionTreeClassifier()
    clf = clf.fit(data_train, y_train)
    predicted = clf.predict(vectorizer.transform(X_test))
    accuracy = measure(y_test, predicted)
    joblib.dump(clf, 'model/gender_predict_DecTree.pkl')
    return accuracy


def predict(name):
    clf = joblib.load('model/gender_predict_DecTree.pkl')
    result = clf.predict(vectorizer.transform(features([name])))
    return result[0]


def main():
    accuracy = train(train_size=0.8)
    nama = stopword("Utsman Fajar S.Kom")
    result = predict(nama)
    print("Akurasi : ", accuracy)
    print("Hasil : ", result)


if __name__ == '__main__':
    main()
