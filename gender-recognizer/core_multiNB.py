import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import metrics
from sklearn.externals import joblib
from checker import *

cv = CountVectorizer()


def load_data(female, male):
    female_names = pd.read_csv(female, names=['name'])
    male_names = pd.read_csv(male, names=['name'])
    female_names['gender'] = 0
    male_names['gender'] = 1
    data = female_names.append(male_names, ignore_index=True)
    data = data.drop_duplicates(subset='name', keep=False)
    Xfeatures = np.array(data['name'])
    y = np.array(data['gender'])
    return Xfeatures, y


def data_split(x_features, y_target, train_size):
    X_train, X_test, y_train, y_test = train_test_split(
        x_features, y_target, train_size=train_size)
    return X_train, X_test, y_train, y_test


def features(name):
    return "{name[-1]} {name[-2:]} {name[-3:]}"


def feature_extraction():
    data_female = 'gender/class/female.txt'
    data_male = 'gender/class/male.txt'
    Xfeatures, y = load_data(data_female, data_male)
    for i in range(len(Xfeatures)):
        Xfeatures[i] = features(Xfeatures[i])
    X = cv.fit_transform(Xfeatures)
    return X, y


def measure(target_test, predicted):
    return metrics.accuracy_score(target_test, predicted)


def train(train_size):
    Xfeatures, y_target = feature_extraction()
    X_train, X_test, y_train, y_test = data_split(
        Xfeatures, y_target, train_size=train_size)
    clf = MultinomialNB()
    clf = clf.fit(X_train, y_train)
    predicted = clf.predict(X_test)
    accuracy = measure(y_test, predicted)
    joblib.dump(cv.vocabulary_, 'model/vectorizer_vocab.pkl')
    joblib.dump(clf, 'model/gender_predict_multiNB.pkl')
    return accuracy


def predict_to_string(result):
    if result[0] == 1:
        return ("Male")
    else:
        return ("Female")


def predict(name):
    cv = CountVectorizer(vocabulary=joblib.load('model/vectorizer_vocab.pkl'))
    clf = joblib.load('model/gender_predict_multiNB.pkl')
    #cv = joblib.load('model/vectorizer_vocab.pkl')
    pred_name = np.array([name])
    pred_name[0] = features(pred_name[0])
    pred_name_vector = cv.transform(pred_name)
    result = predict_to_string(clf.predict(pred_name_vector))
    return result


def main():
    accuracy = train(train_size=0.8)
    nama = stopword("Jessica")
    result = predict(nama)
    print("Akurasi : ", accuracy)
    print("Hasil : ", result)


if __name__ == '__main__':
    main()
