import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
# Extracting features from text files
from sklearn.feature_extraction.text import CountVectorizer

# TF-IDF
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import metrics

# Machine Learning
# Importando clasificador Naive Bayes (NB).
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV


def load_data(female, male):
    female_names = pd.read_csv(female, names=['name'])
    male_names = pd.read_csv(male, names=['name'])
    female_names['gender'] = 0
    male_names['gender'] = 1
    data = female_names.append(male_names, ignore_index=True)
    data = data.drop_duplicates(subset='name', keep=False)
    target = data['gender'].astype(str)
    features = data['name'].astype(str)
    return target, features


def data_split(target, features, test_size):
    X_train, X_test, y_train, y_test = train_test_split(
        features, target, random_state=7, test_size=test_size)
    return X_train, X_test, y_train, y_test


def train(data_train, target_train):
    text_clf = Pipeline([('vect', CountVectorizer(ngram_range=(1, 1))), ('tfidf', TfidfTransformer(
        norm='l2', sublinear_tf=True, use_idf=False)), ('clf', MultinomialNB(alpha=0.1))])
    text_clf = text_clf.fit(data_train, target_train)
    return text_clf


def predict_to_string(result):
    if result[0] == '1':
        return ("Male")
    else:
        return ("Female")


def measure(target_test, predicted):
    return metrics.accuracy_score(target_test, predicted)


def grid_search(clf, x_train, y_train):
    parameters = {
        # 'vect__max_df': (0.5, 0.625, 0.75, 0.875, 1.0),
        # 'vect__max_features': (None, 5000, 10000, 20000),
        # 'vect__min_df': (1, 5, 10, 20, 50),
        'vect__ngram_range': [(1, 1), (1, 2)],
        'tfidf__use_idf': (True, False),
        'tfidf__sublinear_tf': (True, False),
        # 'vect__binary': (True, False),
        'tfidf__norm': ('l1', 'l2'),
        'clf__alpha': (1, 0.1, 0.01, 0.001, 0.0001, 0.00001)
    }

    gs_clf = GridSearchCV(clf, parameters, n_jobs=-1, cv=2)
    gs_clf = gs_clf.fit(x_train, y_train)
    return gs_clf


def main():
    data_female = 'gender/class/female.txt'
    data_male = 'gender/class/male.txt'
    target, features = load_data(data_female, data_male)
    X_train, X_test, y_train, y_test = data_split(
        target, features, test_size=0.2)
    clf = train(X_train, y_train)
    gs_clf = grid_search(clf, X_train, y_train)
    prediksiGS = gs_clf.predict(X_test)
    akurasi = measure(y_test, prediksiGS)
    print("Akurasi: ", akurasi)
    print("Hasil prediksi: ")
    hasil = predict_to_string(gs_clf.predict(("Ahmed", )))
    print("nama Minami, gender: ", hasil)


if __name__ == '__main__':
    main()
