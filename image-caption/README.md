Image Captioning
Give caption into image

Requirements :
- Python 3
- Keras == 2.1.6
- Numpy
- tqdm
- Tensorflow == 1.5
- Falcon
- Gunicorn

Dataset : Flickr8K

Cara Penggunaan :
- Masuk ke python cli
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Training :
- Buat folder data. copy semua data Flickr8k ke folder data
- Jalankan preprocessing.py sampai selesai
- Jalankan train.py samapi selesai

Cara Testing API dengan postman:
- isi url dengan localhost:8890/api/predict dan ganti method menjadi POST
- pada tab body pilih form-data dan isikan kolom key dengan image dan type file
- pilih gambar melalui kolom value
