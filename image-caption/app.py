import falcon
import json
import os
import io
import falcon_jsonify
from image_captioning import *
from keras import backend as K
from falcon_multipart.middleware import MultipartMiddleware


class HelloWorldResource:
    def on_get(self, req, resp, foo):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ("Hello " + foo +
                     ".  Python's Falcon thinks you are awesome!")


class DetekResource:
    _storage_path = './temp'

    def on_post(self, req, resp):
        """Handles POST requests"""
        image = req.get_param('image')
        
        if image != None:
            raw = image.file.read()
            
            filename = image.filename 
            
            file_path = os.path.join(self._storage_path, filename)
            
            temp_file_path = file_path + '~'

            open(temp_file_path, 'wb').write(raw)
            
            os.rename(temp_file_path, file_path)

            result = get_captions(file_path)
            K.clear_session()
            resp.status = falcon.HTTP_200
            resp.body = (json.dumps(
                        {'status': "success", 'result': result}))
        else:
            resp.status = falcon.HTTP_404
            resp.body = (json.dumps(
                        {'status': "error", 'result': 'upload gambar dahulu'}))


app = falcon.API(middleware=[MultipartMiddleware()])
app.req_options.auto_parse_form_urlencoded = True

app.add_route('/hello/{foo}', HelloWorldResource())
app.add_route('/api/predict', DetekResource())
