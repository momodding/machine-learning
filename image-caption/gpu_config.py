import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


def set_config(gpu_memory=0.6, gpu_device="0"):
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = gpu_memory
    config.gpu_options.visible_device_list = gpu_device
    set_session(tf.Session(config=config))
