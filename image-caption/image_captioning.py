from copy import deepcopy
from heapq import nsmallest
from caption_utils import *
from tqdm import tqdm
from keras.models import load_model
from keras.preprocessing import image
from gpu_config import set_config


def duplicate_bottleneck_features(features):
    num_captions = 5
    num_rows = features.shape[0] * num_captions

    features_dup = np.zeros((num_rows, features.shape[1]))
    for i, image in enumerate(features):
        for j in range(num_captions):
            features_dup[i*num_captions + j] = image
    return features_dup


def seq_to_sentence(sent):
    return ' '.join([idx2token[idx] for idx in sent])


def generate_seq(img_input, alpha=1.):
    if img_input.shape != (1, 512):
        img_input = img_input.reshape(1, 512)

    assert (img_input.shape == (1, 512))
    stop_condition = False
    decoded_sentence = []
    target_seq = np.array([token2idx['<bos>']]).reshape(1, 1)
    states_value = encoder_model.predict(img_input)

    neg_log_proba = 0.
    while not stop_condition:

        output_tokens, h, c = decoder_model.predict([target_seq] +
                                                    states_value)
        output_tokens = np.squeeze(output_tokens)

        sampled_token_index = int(np.argmax(output_tokens))
        neg_log_proba -= np.log(output_tokens[sampled_token_index])

        sampled_char = idx2token[sampled_token_index]

        decoded_sentence += [sampled_char]

        if (sampled_char == '<eos>' or len(decoded_sentence) > 30):
            stop_condition = True

        target_seq = np.array([sampled_token_index]).reshape(1, 1)

        states_value = [h, c]
        neg_log_proba /= len(decoded_sentence)**alpha
    return ' '.join(decoded_sentence[:-1])


def decoder_sequence(sent, decoder_model, beam_size=5, len_norm=True, alpha=1):
    """
    sent: ([neg_log_prob, [1, ...]], [h, c])
    states_value: [h, c]
    return list of sent
    """
    prev_log_prob = sent[0][0]
    prev_sent = sent[0][1]
    last_word_idx = prev_sent[-1]
    states_value = sent[1]

    assert last_word_idx not in (token2idx['<eos>'], token2idx['<unk>'])

    target_seq = np.array([last_word_idx]).reshape(1, 1)
    output_tokens, h, c = decoder_model.predict([target_seq] + states_value)
    output_tokens = np.squeeze(output_tokens)

    predicted_sentences = []
    output_tokens_beam = np.argsort(-output_tokens)
    output_tokens_beam = list(
        filter(lambda x: x not in [0, 1, 3], output_tokens_beam))[:beam_size]

    assert len(output_tokens_beam) == beam_size

    for predict_idx in output_tokens_beam:
        new_sent = prev_sent + [int(predict_idx)]

        if len_norm:
            neg_log_prob = prev_log_prob * \
                max(len(prev_sent)-1, 1)**alpha - \
                np.log(output_tokens[int(predict_idx)])
            neg_log_prob /= max(len(new_sent) - 1, 1)**alpha
        else:
            neg_log_prob = prev_log_prob - \
                np.log(output_tokens[int(predict_idx)])

        predicted_sentences.append(([neg_log_prob, new_sent], [h, c]))
    return predicted_sentences


def beam_search(img_input,
                encoder_model,
                decoder_model,
                input_shape,
                beam_size,
                max_length,
                len_norm,
                alpha,
                return_probs):
    """throws an error on beam_size 1 when <unk> is produced"""
    if img_input.shape != (1, input_shape):
        img_input = img_input.reshape(1, input_shape)
    assert (img_input.shape == (1, input_shape))
    states_value_initial = encoder_model.predict(img_input)

    beg_sent_and_states = ([0., [token2idx['<bos>']]], states_value_initial)
    #     print(beg_sent)
    top_sentences = decoder_sequence(beg_sent_and_states, decoder_model,
                                     beam_size, len_norm, alpha)
    #     print(list(map(lambda x: seq_to_sentence(x[1]), top_sentences)))

    stop_condition = False

    while not stop_condition:
        new_top_sentences = []
        for sent in top_sentences:
            if sent[0][1][-1] == token2idx['<eos>']:
                new_top_sentences.append(sent)
                continue

            predicted_sent = decoder_sequence(sent, decoder_model, beam_size,
                                              len_norm, alpha)
            new_top_sentences.extend(predicted_sent)

        top_sentences = sorted(
            new_top_sentences, key=lambda x: x[0][0])[:beam_size]
        assert len(top_sentences) == beam_size

        #         print(seq_to_sentence(top_sentences[0][1]))

        # Update stop condition
        eos_cnt = 0
        any_max_len = False
        for sent in top_sentences:
            if sent[0][1][-1] == token2idx['<eos>']:
                eos_cnt += 1
            if len(sent[0][1]) >= max_length:
                any_max_len = True
                break

        if any_max_len or (eos_cnt == beam_size):
            stop_condition = True

    if return_probs:
        return list(
            map(
                lambda x: seq_to_sentence(x[0][1][1:-1]),
                top_sentences))
    else:
        return list(
            map(lambda x: seq_to_sentence(x[0][1][1:-1]), top_sentences))[0]


def get_image_features(img_path):
    from keras.applications.inception_v3 import InceptionV3
    from keras.applications.inception_v3 import preprocess_input
    pretrained_model = InceptionV3(
        weights='imagenet', include_top=False, pooling='avg')

    img = image.load_img(img_path, target_size=(299, 299))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    features = pretrained_model.predict(x)
    return features


train_fns_list, dev_fns_list, test_fns_list = load_split_lists()
train_captions_raw, dev_captions_raw, test_captions_raw = get_caption_split()
vocab = create_vocab(train_captions_raw)
token2idx, idx2token = vocab_to_index(vocab)


def get_captions(img_path):

    # set_config()

    input_shape = 2048

    encoder_model = load_model(
        'saved_models/encoder_model_InceptionV3_avg_norm.h5')
    decoder_model = load_model(
        'saved_models/decoder_model_InceptionV3_avg_norm.h5')
    beam_size = 5
    max_length = 20
    len_norm = True
    alpha = 0.7
    return_probs = False
    # beam_size = 7
    # max_length = 15
    # len_norm = True
    # alpha = 1.2
    # return_probs = False

    img_input = get_image_features(img_path)

    generated_captions = beam_search(
        img_input,
        encoder_model=encoder_model,
        decoder_model=decoder_model,
        input_shape=input_shape,
        beam_size=beam_size,
        max_length=max_length,
        len_norm=len_norm,
        alpha=alpha,
        return_probs=return_probs)
    return generated_captions


if __name__ == "__main__":
    print(get_captions("/Applications/MAMP/htdocs/ci-api1/gambar/12830823_87d2654e31.jpg"))
    # print(get_captions("data/Flicker8k_Dataset/35506150_cbdb630f4f.jpg"))
