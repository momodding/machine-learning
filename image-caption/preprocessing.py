import glob
import os
import json
import numpy as np
from keras.preprocessing import image
from keras.utils import to_categorical
from tqdm import tqdm

from caption_utils import *
from gpu_config import set_config


def get_filenames(txtfile):
    with open(txtfile) as f:
        file_paths = []
        for line in f:
            file_paths.append('data/Flicker8k_Dataset/' + line.rstrip())
    return file_paths


def generate_json(tokenidx, filename):
    if os.path.exists(filename):
        os.remove(filename)
    output = open(filename, "a+")
    output.write(json.dumps(tokenidx))
    output.close()


def generate_bottleneck_features(model, dim_last_layer, filename_list):
    bottleneck_features = np.zeros((len(filename_list), dim_last_layer))
    for i, path in enumerate(tqdm(filename_list, unit='files')):
        img = image.load_img(path, target_size=(299, 299))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        features = model.predict(x)
        bottleneck_features[i] = features
    return bottleneck_features


def one_hot_encode(fns_list, caption_dictionary, num_captions_per_image, max_words_in_sentence, num_words):
    total_captions = len(caption_dictionary) * num_captions_per_image
    captions_onehot_processed = np.zeros(
        (total_captions, max_words_in_sentence, num_words)).astype(bool)
    for i, filename in enumerate(tqdm(fns_list)):
        for j, caption in enumerate(caption_dictionary[filename]):
            onehot = to_categorical(caption, num_classes=len(vocab))
            for k, word in enumerate(onehot):
                captions_onehot_processed[i*num_captions_per_image+j][k] = word
    return captions_onehot_processed


def duplicate_bottleneck_features(features):
    num_captions = 5
    num_rows = features.shape[0] * num_captions

    features_dup = np.zeros((num_rows, features.shape[1]))
    for i, image in enumerate(features):
        for j in range(num_captions):
            features_dup[i*num_captions + j] = image
    return features_dup


def captions_onehot_split(captions_onehot):
    """ returns decoder input data and decoder target data """
    return captions_onehot[:, :-1, :], captions_onehot[:, :, :]


if __name__ == '__main__':
    set_config()

    #############################################################
    # Preprocesing 1
    #############################################################

    # Initialize bottleneck_features
    train_files = get_filenames('data/Flickr8k_text/Flickr_8k.trainImages.txt')
    test_files = get_filenames('data/Flickr8k_text/Flickr_8k.testImages.txt')
    validation_files = get_filenames(
        'data/Flickr8k_text/Flickr_8k.devImages.txt')

    print("Number of train files: {}".format(len(train_files)))
    print("Number of test files: {}".format(len(test_files)))
    print("Number of validation files: {}".format(len(validation_files)))

    # InceptionV3 (last layer: 2048)
    print("\nGenerating bottleneck features for InceptionV3 ...")

    from keras.applications.inception_v3 import InceptionV3
    from keras.applications.inception_v3 import preprocess_input

    model = InceptionV3(weights='imagenet', include_top=False, pooling='avg')

    bottleneck_features_train = generate_bottleneck_features(
        model, 2048, train_files)
    bottleneck_features_test = generate_bottleneck_features(
        model, 2048, test_files)
    bottleneck_features_validation = generate_bottleneck_features(
        model, 2048, validation_files)

    print("\nSaving bottleneck features for InceptionV3 ...")
    np.savez('bottleneck_features/bottleneck_features_InceptionV3_avgpooling_kantor',
             train=bottleneck_features_train,
             test=bottleneck_features_test,
             validation=bottleneck_features_validation)

    #############################################################
    # Preprocesing 2
    #############################################################

    train_filename_list, validation_filename_list, test_filename_list = load_split_lists()

    train_captions_raw, validation_captions_raw, test_captions_raw = get_caption_split()
    vocab = create_vocab(train_captions_raw)
    token2idx, idx2token = vocab_to_index(vocab)
    captions_data = (train_captions_raw.copy(),
                     validation_captions_raw.copy(), test_captions_raw.copy())
    train_captions, validation_captions, test_captions = process_captions(
        captions_data, token2idx)

    print("\nGenerating vocab json file...")
    generate_json(token2idx, "vocab.json")
    print("\nGenerating token2idx json file...")
    generate_json(token2idx, "token2idx.json")
    print("\nGenerating token2idx json file...")
    generate_json(token2idx, "idx2token.json")

    print("\nTotal vocabularies in our dictionary: {}".format(len(vocab)))

    # Calculate the caption with maximum number of words
    caption_lengths = []
    for five_captions in train_captions.values():
        caption_lengths.extend([len(caption) for caption in five_captions])
    for five_captions in validation_captions.values():
        caption_lengths.extend([len(caption) for caption in five_captions])

    max_words_in_sentence = max(caption_lengths)

    print("\nThere are {} number of captions in total.".format(len(caption_lengths)))
    print("The maximum words in a sentence is {}".format(max_words_in_sentence))

    # Save train captions
    num_words = len(vocab)
    num_captions_per_image = 5  # 5 stands for number of captions per image

    print("\nGenerating onehot vectors for train captions...")
    train_captions_onehot_processed = one_hot_encode(
        train_filename_list, train_captions, num_captions_per_image, max_words_in_sentence, num_words)

    print("\nGenerating onehot vectors for validation captions...")
    validation_captions_onehot_processed = one_hot_encode(
        validation_filename_list, validation_captions, num_captions_per_image, max_words_in_sentence, num_words)

    print("\nSaving the preprocessed captions...")
    np.savez('preprocessed_captions/Flicker8k_onehot_' + str(len(vocab)) + '_words',
             train=train_captions_onehot_processed,
             validation=validation_captions_onehot_processed)

    #############################################################
    # Preprocesing 3
    #############################################################

    # Caption Preprocessing
    print("\nLoading preprocessed captions...")

    train_captions_onehot = train_captions_onehot_processed
    validation_captions_onehot = validation_captions_onehot_processed

    train_captions_onehot = train_captions_onehot.astype(np.float32)
    validation_captions_onehot = validation_captions_onehot.astype(np.float32)

    print("\nGenerating Decorder input and target data")

    train_decoder_input, train_decoder_target = captions_onehot_split(
        train_captions_onehot)
    validation_decoder_input, validation_decoder_target = captions_onehot_split(
        validation_captions_onehot)

    train_decoder_input = np.argmax(train_decoder_input, axis=-1)
    validation_decoder_input = np.argmax(validation_decoder_input, axis=-1)

    print("\nDecoder Input shape: {}, dtype: {}".format(
        train_decoder_input.shape, train_decoder_input.dtype))
    print("Decoder Target shape: {}, dtype: {}".format(
        train_decoder_target.shape, train_decoder_target.dtype))
    # print("Encoder Output shape: {}, dtype: {}".format(
    #     train_encoder_output.shape, train_encoder_output.dtype))

    print("\nSaving the final data to be used directly for training...")
    np.savez('train_dev_avg_kantor',
             train_decoder_input=train_decoder_input,
             train_decoder_target=train_decoder_target,
             validation_decoder_input=validation_decoder_input,
             validation_decoder_target=validation_decoder_target)
