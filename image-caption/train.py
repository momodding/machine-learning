import numpy as np
from argparse import ArgumentParser
from keras.models import Model, Sequential
from keras.layers import Input, Dense, BatchNormalization, RepeatVector, Concatenate, merge, Masking
from keras.layers import LSTM, GRU, Embedding, TimeDistributed, Bidirectional
from keras import backend as K
from keras import optimizers
from keras.callbacks import ModelCheckpoint
from keras import backend as K
from keras.utils import plot_model
import matplotlib.pyplot as plt

from caption_utils import *
from gpu_config import set_config


# Since there are 5 captions per image, duplicate the bottleneck features
def duplicate_bottleneck_features(features):
    num_captions = 5  # 5 stands for number of captions per image
    num_rows = features.shape[0] * num_captions

    features_dup = np.zeros((num_rows, features.shape[1]))
    for i, image in enumerate(features):
        for j in range(num_captions):
            features_dup[i*num_captions + j] = image
    return features_dup


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))


if __name__ == "__main__":

    emb_size = 300
    lstm_size = 300
    learning_rate = 0.001
    # learning_rate = 0.0001
    dropout_rate = 0.2
    batch_size = 32
    n_epochs = 20

    trained_model = "InceptionV3_avg_norm.h5"
    encoder_model = "encoder_model_InceptionV3_avg_norm.h5"
    decoder_model = "decoder_model_InceptionV3_avg_norm.h5"

    set_config()

    # Load Dataset
    print("\nLoading train_dev_avg.npz...")
    all_data = np.load('train_dev_avg_kantor.npz')

    train_decoder_input = all_data['train_decoder_input']
    train_decoder_target = all_data['train_decoder_target'][:, 1:, :]
    validation_decoder_input = all_data['validation_decoder_input']
    validation_decoder_target = all_data['validation_decoder_target'][:, 1:, :]

    # Load Bottleneck Feature
    bottleneck_features = np.load(
        'bottleneck_features/bottleneck_features_InceptionV3_avgpooling_kantor.npz')

    # Split Bottleneck Features
    bottleneck_features_train = bottleneck_features["train"]
    bottleneck_features_validation = bottleneck_features["validation"]

    # Duplicate Bottleneck features depend on number of captions
    train_encoder_output = duplicate_bottleneck_features(
        bottleneck_features_train)
    validation_encoder_output = duplicate_bottleneck_features(
        bottleneck_features_validation)

    # Emptying variable
    del all_data
    del bottleneck_features
    del bottleneck_features_train
    del bottleneck_features_validation

    # Define Input Shape
    input_shape = 2048

    # Define checkpointer for saving best weights
    checkpointer = ModelCheckpoint(filepath='saved_models/weights.best.InceptionV3.avg.final.hdf5',
                                   verbose=1, save_best_only=True)
    path_best_weights = 'saved_models/weights.best.InceptionV3.avg.final.hdf5'

    # Define path location for saving model file
    path_model = "saved_models/" + trained_model
    path_encoder_model = "saved_models/" + encoder_model
    path_decoder_model = "saved_models/" + decoder_model

    print("\nTrain Decoder Input", train_decoder_input.shape,
          train_decoder_input.dtype)
    print("Train Decoder Target", train_decoder_target.shape,
          train_decoder_target.dtype)
    print("Train Encoder Output", train_encoder_output.shape,
          train_encoder_output.dtype)

    train_fns_list, dev_fns_list, test_fns_list = load_split_lists()

    train_captions_raw, dev_captions_raw, test_captions_raw = get_caption_split()
    vocab = create_vocab(train_captions_raw)
    token2idx, idx2token = vocab_to_index(vocab)
    captions_data = (train_captions_raw.copy(),
                     dev_captions_raw.copy(), test_captions_raw.copy())
    train_captions, dev_captions, test_captions = process_captions(
        captions_data, token2idx)

    # Build the model
    vocab_size = len(vocab)
    max_length = train_decoder_target.shape[1]

    K.clear_session()

    # Image to Image embedding
    image_input = Input(
        shape=(train_encoder_output.shape[1], ), name='image_input')
    print("\nImage input shape: {}".format(image_input.shape))
    img_emb = Dense(emb_size, activation='relu',
                    name='img_embedding')(image_input)
    print("Image embedding shape: {}".format(img_emb.shape))

    # Sentence to Word embedding
    caption_inputs = Input(shape=(None, ), name='caption_input')
    print("\nCaption Input Shape", caption_inputs.shape)
    emb_layer = Embedding(input_dim=vocab_size,
                          output_dim=emb_size, name='Embedding')
    word_emb = emb_layer(caption_inputs)
    print("Word embedding shape: {}".format(word_emb.shape))

    # Define decoder cell in LSTM networks
    decoder_cell = LSTM(lstm_size, return_sequences=True, return_state=True,
                        name='decoder', dropout=dropout_rate, recurrent_dropout=dropout_rate)

    # Define encoder states before training
    encoder_states = [img_emb, img_emb]
    decoder_out, _, _ = decoder_cell(word_emb, initial_state=encoder_states)

    # Wrapping layer for LSTM
    output_layer = TimeDistributed(Dense(vocab_size, activation='softmax'))
    decoder_out = output_layer(decoder_out)

    # Define optimizer function
    rmsprop = optimizers.RMSprop(
        lr=learning_rate, rho=0.9, epsilon=None, decay=1e-6)
    model = Model(inputs=[image_input, caption_inputs], outputs=[decoder_out])

    model.compile(optimizer=rmsprop,
                  loss='categorical_crossentropy',
                  metrics=['accuracy', f1])

    print("\nTraining model summary:")
    print(model.summary())

    # Start Training model
    training = model.fit([train_encoder_output, train_decoder_input], [train_decoder_target],
                         validation_data=([validation_encoder_output, validation_decoder_input], [
                             validation_decoder_target]),
                         epochs=n_epochs, batch_size=batch_size, callbacks=[checkpointer], verbose=2)

    # Inference encoder and decoder model
    encoder_model = Model(image_input, encoder_states)
    print("\nInference model - encoder summary:")
    print(encoder_model.summary())

    decoder_state_input_h = Input(shape=(lstm_size, ))
    decoder_state_input_c = Input(shape=(lstm_size, ))
    decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
    decoder_outputs, state_h, state_c = decoder_cell(
        word_emb, initial_state=decoder_states_inputs)
    decoder_states = [state_h, state_c]
    decoder_outputs = output_layer(decoder_outputs)
    decoder_model = Model(
        [caption_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states)

    print("\nInference model - decoder summary:")
    print(decoder_model.summary())

    # Load the model with the best validation loss
    model.load_weights(path_best_weights)

    # Save the models
    # model.compile(optimizer='rmsprop', loss='mean_squared_error')
    model.save(path_model, include_optimizer=False)
    encoder_model.save(path_encoder_model)
    decoder_model.save(path_decoder_model)
    # plot_model(decoder_model, 'decoder.jpg')

    # list all data in history
    print(training.history.keys())
    # summarize history for accuracy
    # plt.plot(training.history['acc'])
    # plt.plot(training.history['val_acc'])
    # plt.title('model accuracy')
    # plt.ylabel('accuracy')
    # plt.xlabel('epoch')
    # plt.legend(['train', 'val'], loc='upper left')
    # plt.show()
    # summarize history for loss
    plt.plot(training.history['loss'])
    plt.plot(training.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.show()
    # summarize history for F1-score
    # plt.plot(training.history['f1'])
    # plt.plot(training.history['val_f1'])
    # plt.title('model F1')
    # plt.ylabel('f1')
    # plt.xlabel('epoch')
    # plt.legend(['train', 'val'], loc='upper left')
    # plt.show()
