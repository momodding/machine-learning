<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facial extends CI_Controller
{
    public $API ="";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_api');
    }

    public function predict()
    {
        // $request_body = file_get_contents('php://input');
        // $data = (array)json_decode($request_body);
        //$acc_id = $_POST['account_id'];
        //$project_code = $_POST['project_code'];
        //$service_code = $_POST['service_code'];
        //$image = $_FILES['images']['tmp_name'];

        $acc_id = $this->input->post('account_id');
        $project_code = $this->input->post('project_code');
        $service_code = $this->input->post('service_code');
        $image = $this->input->post('images');

        //echo $article;
        //print_r($data);
        print_r($image);
        // $inputan = urldecode($inputan);
        // $inputan = str_replace("\n", "", $inputan);
        // $inputan = str_replace("\"", "", $inputan);
        $data = array();
        $result_api = array();
        $get_ups = $this->Model_api->get_api($acc_id, $project_code, $service_code);
        // var_dump($get_ups);
        if ($get_ups) {
            $ups_api_key = $get_ups[0]['ups_api_key'];
            $ups_api_status = $get_ups[0]['ups_api_status'];
        } else {
            $ups_api_key = "NULL";
            $ups_api_status = "NULL";
        }

        if ($ups_api_key != "NULL") {
            if ($ups_api_status == "active") {
                if ($image != "NULL") {
                    $config['upload_path']          = './gambar/';
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['overwrite']        	= true;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (! $this->upload->do_upload('images')) {
                        $error = array('error' => $this->upload->display_errors());
                        //var_dump($error["error"]);
                        $data["status"] = S_W001;
                        $data["error"] = $error["error"];
                        $result = json_encode($data);
                        $this->output->set_content_type('application/json')->set_output($result);
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        // var_dump($data['upload_data']["file_name"]);
                        $gambar = $data['upload_data']["full_path"];
                        $command = "curl -F \"image=@$gambar\" " . KINI_FACIAL_EMOTION . "predict";
                        // $result = $this->restclient->dexec($command);
                        $result = shell_exec($command);
                        $json = (array) json_decode($result);

                        // $json = json_decode($json, true);
                        //echo "JSON</br>";
                        // print_r($json);
                        // print_r($command);
                        // var_dump($json);

                        // echo $command."<br>";

                        // passing the parameter into flask API
                        //$jsonResult = (array) $json['result'];
                        $result_api["result"] = $json["result"];
                        $result_api["status"] = $json['status'];
                        $result_api["filename"] = $data['upload_data']['file_name'];
                        $result_api["filepath"] = $data['upload_data']["full_path"];
                        //$data["command"] = $command;
                        $data["status"] = S_S001;
                        $data["message"] = M_S001;

                        $simpan = array('account_id'        => $acc_id,
                                        'up_code'           => $project_code,
                                        'ups_code'          => $service_code,
                                        'lapc_name'         => "facial_emotion",
                                        'lapc_access_status'=> $result_api["status"],
                                        'lapc_input'             => $data['upload_data']['file_name'],
                                        'lapc_output'            => json_encode($result_api)
                        );

                        $this->simpan_data($simpan);

                        $result = json_encode($result_api);
                        $this->output->set_content_type('application/json')->set_output($result);
                    }
                } else {
                    $data["status"] = S_W001;
                    $data["error"] = M_W003;
                    $result = json_encode($data);
                    $this->output->set_content_type('application/json')->set_output($result);
                }
            } else {
                $data["status"] = S_W001;
                $data["error"] = M_W001;
                $result = json_encode($data);
                $this->output->set_content_type('application/json')->set_output($result);
            }
        } else {
            $data["status"] = S_E001;
            $data["error"] = M_E001;
            $result = json_encode($data);
            $this->output->set_content_type('application/json')->set_output($result);
        }
    }

    public function train()
    {
        $request_body = file_get_contents('php://input');
        $data = (array)json_decode($request_body);
        $acc_id = $data['account_id'];
        $project_code = $data['project_code'];
        $service_code = $data['service_code'];
        // $session = $data['session'];
        $train_size = $data['train_size'];
        $batch_size = $data['batch_size'];
        $epochs = $data['epochs'];

        //echo $article;
        //print_r($data);

        $data = array();
        $result_api = array();
        $get_ups = $this->Model_api->get_api($acc_id, $project_code, $service_code);
        // var_dump($get_ups);
        if ($get_ups) {
            $ups_api_key = $get_ups[0]['ups_api_key'];
            $ups_api_status = $get_ups[0]['ups_api_status'];
        } else {
            $ups_api_key = "NULL";
            $ups_api_status = "NULL";
        }

        if ($ups_api_key != "NULL") {
            if ($ups_api_status == "active") {
                if ($size != "NULL" && $engine != "NULL") {
                    $command = "curl -F \"train_size=$train_size\" \"batch_size=$batch_size\" \"epochs=$epochs\" " . KINI_FACIAL_EMOTION . "train";
                    // $result = $this->restclient->dexec($command);
                    $result = shell_exec($command);
                    $json = (array) json_decode($result);

                    // $json = json_decode($json, true);
                    //echo "JSON</br>";
                    // print_r($json);
                    // var_dump($json);

                    // echo $command."<br>";

                    // passing the parameter into flask API
                    //$jsonResult = (array) $json['result'];
                    $result_api["akurasi"] = $json["akurasi"];
                    $result_api["status"] = $json['status'];
                    //$data["command"] = $command;
                    $data["status"] = S_S001;
                    $data["message"] = M_S001;

                    $simpan = array('account_id'        => $acc_id,
                                    'up_code'           => $project_code,
                                    'ups_code'          => $service_code,
                                    'lapc_name'         => "gender_recognizer",
                                    'lapc_access_status'=> $result_api["status"],
                                    'lapc_input'             => json_encode(array($size,$engine)),
                                    'lapc_output'            => json_encode($result_api)
                    );

                    $this->simpan_data($simpan);

                    $result = json_encode($result_api);
                    $this->output->set_content_type('application/json')->set_output($result);
                } else {
                    $data["status"] = S_W001;
                    $data["error"] = M_W003;
                    $result = json_encode($data);
                    $this->output->set_content_type('application/json')->set_output($result);
                }
            } else {
                $data["status"] = S_W001;
                $data["error"] = M_W001;
                $result = json_encode($data);
                $this->output->set_content_type('application/json')->set_output($result);
            }
        } else {
            $data["status"] = S_E001;
            $data["error"] = M_E001;
            $result = json_encode($data);
            $this->output->set_content_type('application/json')->set_output($result);
        }
    }

    public function simpan_data($data)
    {
        $this->Model_api->simpan($data);
    }
}
