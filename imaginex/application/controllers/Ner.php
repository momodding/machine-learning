<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ner extends CI_Controller
{
    public $API ="";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_api');
    }

    public function predict()
    {
        $request_body = file_get_contents('php://input');
        $data = (array)json_decode($request_body);
        $acc_id = $data['account_id'];
        $project_code = $data['project_code'];
        $service_code = $data['service_code'];
        // $session = $data['session'];
        $inputan = $data['input'];
        $engine = $data['engine'];

        //echo $article;
        //print_r($data);

        $inputan = urldecode($inputan);
        $inputan = str_replace("\n", "", $inputan);
        $inputan = str_replace("\"", "", $inputan);
        $data = array();
        $result_api = array();
        $get_ups = $this->Model_api->get_api($acc_id, $project_code, $service_code);
        // var_dump($get_ups);
        if ($get_ups) {
            $ups_api_key = $get_ups[0]['ups_api_key'];
            $ups_api_status = $get_ups[0]['ups_api_status'];
        } else {
            $ups_api_key = "NULL";
            $ups_api_status = "NULL";
        }

        if ($ups_api_key != "NULL") {
            if ($ups_api_status == "active") {
                if ($inputan != "NULL" && $engine != "NULL") {
                    $command = "curl -H \"Content-Type: application/json\" -X POST -d '{\"input\":\"$inputan\", \"engine\":\"$engine\"}' " . KINI_NER_DETECTOR . "predict";
                    // $result = $this->restclient->dexec($command);
                    $result = shell_exec($command);
                    $json = (array) json_decode($result);

                    // $json = json_decode($json, true);
                    //echo "JSON</br>";
                    // print_r($json);
                    // var_dump($json);

                    // echo $command."<br>";

                    // passing the parameter into flask API
                    //$jsonResult = (array) $json['result'];
                    $result_api["prediksi"] = $json["prediksi"];
                    $result_api["status"] = $json["status"];
                    //$data["command"] = $command;
                    $data["status"] = S_S001;
                    $data["message"] = M_S001;

                    $simpan = array('account_id'        => $acc_id,
                                    'up_code'           => $project_code,
                                    'ups_code'          => $service_code,
                                    'lapc_name'         => "ner",
                                    'lapc_access_status'=> $result_api["status"],
                                    'lapc_input'             => json_encode(array($inputan,$engine)),
                                    'lapc_output'            => json_encode($result_api)
                    );

                    $this->simpan_data($simpan);

                    $result = json_encode($result_api);
                    $this->output->set_content_type('application/json')->set_output($result);
                } else {
                    $data["status"] = S_W001;
                    $data["error"] = M_W003;
                    $result = json_encode($data);
                    $this->output->set_content_type('application/json')->set_output($result);
                }
            } else {
                $data["status"] = S_W001;
                $data["error"] = M_W001;
                $result = json_encode($data);
                $this->output->set_content_type('application/json')->set_output($result);
            }
        } else {
            $data["status"] = S_E001;
            $data["error"] = M_E001;
            $result = json_encode($data);
            $this->output->set_content_type('application/json')->set_output($result);
        }
    }

    public function train()
    {
        $request_body = file_get_contents('php://input');
        $data = (array)json_decode($request_body);
        $acc_id = $data['account_id'];
        $project_code = $data['project_code'];
        $service_code = $data['service_code'];
        // $session = $data['session'];
        $size = $data['size'];
        $engine = $data['engine'];

        //echo $article;
        //print_r($data);

        $data = array();
        $result_api = array();
        $get_ups = $this->Model_api->get_api($acc_id, $project_code, $service_code);
        // var_dump($get_ups);
        if ($get_ups) {
            $ups_api_key = $get_ups[0]['ups_api_key'];
            $ups_api_status = $get_ups[0]['ups_api_status'];
        } else {
            $ups_api_key = "NULL";
            $ups_api_status = "NULL";
        }

        if ($ups_api_key != "NULL") {
            if ($ups_api_status == "active") {
                if ($size != "NULL" && $engine != "NULL") {
                    $command = "curl -H \"Content-Type: application/json\" -X POST -d '{\"size\":$size, \"engine\":\"$engine\"}' " . KINI_NER_DETECTOR . "train";
                    // $result = $this->restclient->dexec($command);
                    $result = shell_exec($command);
                    $json = (array) json_decode($result);

                    // $json = json_decode($json, true);
                    //echo "JSON</br>";
                    // print_r($json);
                    // var_dump($json);

                    // echo $command."<br>";

                    // passing the parameter into flask API
                    //$jsonResult = (array) $json['result'];
                    $result_api["akurasi"] = $json["akurasi"];
                    $result_api["status"] = $json['status'];
                    //$data["command"] = $command;
                    $data["status"] = S_S001;
                    $data["message"] = M_S001;

                    $simpan = array('account_id'        => $acc_id,
                                    'up_code'           => $project_code,
                                    'ups_code'          => $service_code,
                                    'lapc_name'         => "gender_recognizer",
                                    'lapc_access_status'=> $result_api["status"],
                                    'lapc_input'             => json_encode(array($size,$engine)),
                                    'lapc_output'            => json_encode($result_api)
                    );

                    $this->simpan_data($simpan);

                    $result = json_encode($result_api);
                    $this->output->set_content_type('application/json')->set_output($result);
                } else {
                    $data["status"] = S_W001;
                    $data["error"] = M_W003;
                    $result = json_encode($data);
                    $this->output->set_content_type('application/json')->set_output($result);
                }
            } else {
                $data["status"] = S_W001;
                $data["error"] = M_W001;
                $result = json_encode($data);
                $this->output->set_content_type('application/json')->set_output($result);
            }
        } else {
            $data["status"] = S_E001;
            $data["error"] = M_E001;
            $result = json_encode($data);
            $this->output->set_content_type('application/json')->set_output($result);
        }
    }

    public function simpan_data($data)
    {
        $this->Model_api->simpan($data);
    }
}
