<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * This class contain of all support fuction for development
 * @author : Parama_Fadli_Kurnia
 * Developer can make some additional code in this classs
 */
class Restclient
{
    /* validate gender by user input */

    /* encrypt data with base 64 */

    public function encrypt($q)
    {
        //$cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
        //$qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        $qEncoded = base64_encode($q);
        return($qEncoded);
    }

    /* decrypt data with base 64 */

    public function decrypt($q)
    {
        //$cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
        //$qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        $qDecoded = base64_decode($q);
        return($qDecoded);
    }

    /* get url object */

    public function mcurl($url)
    {
        $result = "";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /* get url object with data */

    public function dcurl($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, "message=some_msg&method=post&access_token=xyz"); // define what you want to post
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params); // define what you want to post
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function pcurl($url, $jsonParams)
    {
        $ch = curl_init('http://api.local/rest/users');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


    // save data into specific file
    public function save_to($session, $content, $file_type)
    {
        $myfile = fopen(DATA . $session . "." . $file_type, "w") or die("Unable to open file!");
        fwrite($myfile, $content);
        fclose($myfile);
    }

    // delete data with json, csv and prf format
    public function delete_file($session)
    {
        $json = DATA . $session . ".json";
        $csv = DATA . $session . ".csv";
        $prf = DATA . $session . ".prf";

        if (file_exists($prf)) {
            unlink($prf);
        }
        if (file_exists($json)) {
            unlink($json);
        }
        if (file_exists($csv)) {
            unlink($csv);
        }
    }

    // read specifi file at specific path or directory
    public function read_file($session)
    {
        $user = "";
        $prf = DATA . $session . ".prf";
        $myfile = fopen($prf, "r") or die("Unable to open file!");
        $user = fgets($myfile);
        fclose($myfile);
        return $user;
    }

    // get web content via curl process
    public function get($url)
    {
        $result = shell_exec(CURL . ' "' . $url . '"');
        $json = (array) json_decode($result);
        return $json;
    }

    // get web content via curl process
    public function dexec($command)
    {
        $result = shell_exec($command);
        $json = (array) json_decode($result);
        return $json;
    }

    // check if key exist in array
    public function validate($key, $array)
    {
        return (array_key_exists($key, $array) ? $array[$key] : "NULL");
    }

    public function get_json_result($command)
    {
        $result = shell_exec(CURL . " " . $command);
        $json = (array) json_decode($result);
        return $json;
    }

    public function make_token()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 24; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $str_pass = implode($pass);

        $numlink = '0123456789';
        $pass_numlink = array(); //remember to declare $pass as an array
        $numlinkLength = strlen($numlink) - 1; //put the length -1 in cache
        for ($i = 0; $i < 16; $i++) {
            $n = rand(0, $numlinkLength);
            $pass_numlink[] = $numlink[$n];
        }
        $str_numlink = implode($pass_numlink);
        $token = $str_pass . "|" . $str_numlink;
        $data = array();
        $data["tk_apps"] = $str_numlink;
        $data["tk_secret"] = $str_pass;
        $data["tk_token"] = $token;
        return $data; //turn the array into a string
    }

    public function clean_email($str_comment)
    {
        $pattern = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
        $replacement = "";
        $new_comment = preg_replace($pattern, $replacement, $str_comment);
        return $new_comment;
    }

    public function get_http_response_code($url)
    {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }

    public function get_hashtags($string)
    {
//        $string = "#sukaOnline #OK #sukaOnline #parama #fadli #kurnia #iingria #parama #parama";
        $unique_word = array_unique(str_word_count(trim($string), 1));
        $unique_word_array = array_values($unique_word);
        $count = count($unique_word);
        $data = array();
        $output = array();

        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                $word = $unique_word_array[$i];
                $word_counter = substr_count($string, $word);
                $len_word = strlen($word);

                if ($len_word > 2) {
                    $input = array();
                    $input["ID"] = $i;
                    $input["key"] = "#" . $word;
                    $input["value"] = $word_counter;
                    $output[$i] = $input;
                }
            }
            # get a list of sort columns and their data to pass to array_multisort
            $sort = array();
            foreach ($output as $k => $v) {
                $sort['key'][$k] = $v['key'];
                $sort['value'][$k] = $v['value'];
            }
            # sort by event_type desc and then title asc
            array_multisort($sort['value'], SORT_DESC, $output);
        }

        return $output;
    }

    public function format_date($time)
    {
        $time_fix = "";
        $before_time_list = explode(" ", (string) $time);
        $time_fix = $before_time_list[0] . "T" . $before_time_list[1] . "Z";
        return $time_fix;
    }
}
