<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Model_api extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }

        public function simpan($data)
        {
            $this->db->insert('log_api_access', $data);
        }

        public function get_api($id, $up, $ups)
        {
            $this->db->select('*');
            $this->db->from('user_project_service');
            $this->db->where('account_id', $id);
            $this->db->where('up_code', $up);
            $this->db->where('ups_code', $ups);
            $query = $this->db->get();
            return $query->result_array();
        }
    }
