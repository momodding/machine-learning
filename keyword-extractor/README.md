Semantic Analysis
Detecting similiarity from two sentence or paragraph

Requirements :
- Python 3
- NLTK
- NLTK punkt, conll2000
- Spacy
- TextBlob
- vaderSentiment
- Falcon
- Gunicorn

Cara Penggunaan :
- Masuk ke python cli
- impot nltk
  nltk.download('conll2000')
  nltk.download('punkt')
  exit()
- python -m spacy download <bahasa yang digunakan>
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Akses API dengan postman:
- isi url dengan localhost:8886/api dan ganti method menjadi POST
- pada tab body pilih raw -> application/json
- pada kolom key isi inputan
- pada kolom value isi inputan dengan teks