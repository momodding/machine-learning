import spacy
import numpy as np
from textblob.np_extractors import ConllExtractor
from textblob import TextBlob
import re
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


def get_sentiment(text, library="textblob"):
    if library == 'textblob':
        return(get_textblob_sentiment(text))
    elif library == 'vader':
        return(get_vader_sentiment(text))
    else:
        return({'sentiment error': 'Invalid sentiment library'})


def get_textblob_sentiment(text):
    analysis = TextBlob(text)
    sentiment = analysis.sentiment
    return({'sentiment.score': sentiment.polarity, 'subjectivity': sentiment.subjectivity})


def get_vader_sentiment(text):
    analyzer = SentimentIntensityAnalyzer()
    result = analyzer.polarity_scores(text)
    result['sentiment.score'] = result['compound']

    return(result)


def get_entities(text, library = "spacy", lang="xx"):
    if library == 'spacy':
        return(get_spacy_entities(text, lang))
    elif library == 'textblob':
        return(get_textblob_entities(text))
    else:
        return({'entities error': 'Invalid entities library'})

def get_spacy_entities(text, lang):
    nlp = spacy.load(lang)

    doc = nlp(text)

    # Find named entities, phrases and concepts
    entities = []
    for entity in doc.ents:
        if entity.text.strip() != '':
            entities.append({
                "text": entity.text.strip(),
                "label": entity.label_,
                "start_char": entity.start_char,
                "end_char": entity.end_char
            })

    return(entities)


def get_textblob_entities(text):
    extractor = ConllExtractor()
    blob = TextBlob(text, np_extractor=extractor)
    entities = []
    for entity in blob.noun_phrases:
        entities.append({ 'text': entity.strip() })
    return(entities)


def analyze_text_block(text,
                       sentiment_library = "textblob",
                       entity_library = "spacy", lang="xx"):
    text = re.sub('\s+', ' ', text)
    text = text.replace("\n", ' ')

    entities_res = get_entities(text, library = entity_library, lang="xx")

    blob = TextBlob(text)
    for sentence in blob.sentences:
        sentence_score = get_sentiment(str(sentence), library = sentiment_library)['sentiment.score']
        sentence = str(sentence).lower()

        for entity in entities_res:
            word = entity['text']
            if word.lower() in sentence:
                if 'sentiment.score' not in entity.keys(): entity['sentiment.score'] = []
                entity['sentiment.score'].append(sentence_score)

    for entity in entities_res:
        if 'sentiment.score' not in entity.keys(): entity['sentiment.score'] = [sentiment_res['sentiment.score']]

        entity['num.sentences'] = len(entity['sentiment.score'])
        entity['sentiment.score'] = np.mean(entity['sentiment.score'])

    result = []
    for i in entities_res :
        result.append(next(iter(i.values())))

    return result, entities_res


def main():
    text = """Jokowi sedang melakukan kunjungan kerja di USA. Selama kunjungan tersebut Presiden Indonesia tersebut mengunjungi Kedutaan Besar USA dan White House."""
    result, entities = analyze_text_block(text, lang='xx')
    print(result)
    print(entities)


if __name__ == '__main__':
    main()
