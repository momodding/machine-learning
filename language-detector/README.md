Language Detector
Detecting language of your sentence or paragraph

Requirements :
- Python 3
- NLTK
- NLTK stopword
- Falcon
- Gunicorn

Cara Penggunaan :
- Masuk ke python cli
- impot nltk
  nltk.download('stopwords')
  exit()
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Akses API dengan postman:
- isi url dengan localhost:8881/api dan ganti method menjadi POST
- pada tab body pilih x-www-form-urlencoded
- pada kolom key isi inputan
- pada kolom value isi teks