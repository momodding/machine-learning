import sys
from string import *
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords


def preproces(text):
    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]
    words = [''.join(c for c in s if c not in punctuation) for s in words]

    return words


def cek_text(text):
    """Cek banyak kata dalam teks"""
    words = preproces(text)

    return len(words)


def hitung_rasio(text):
    """Menghitung rasio kemunculan kata pada teks berdasarkan kata pada stopword"""
    rasio = {}

    words = preproces(text)

    for lang in stopwords.fileids():
        stopwords_set = set(stopwords.words(lang))
        words_set = set(words)
        common_words = words_set.intersection(stopwords_set)

        rasio[lang] = len(common_words)

    return rasio


def deteksi(text):
    """Memprediksi bahasa utama dan menghitung probabilitasnya"""
    rasio = hitung_rasio(text)

    bahasa = []
    kata_terbanyak = []
    prob = []

    for i in range(5):
        prediksi_bahasa_utama = max(rasio, key=rasio.get)
        prediksi_kata_terbanyak = rasio[prediksi_bahasa_utama]

        bahasa.append(prediksi_bahasa_utama)
        kata_terbanyak.append(prediksi_kata_terbanyak)

        del rasio[prediksi_bahasa_utama]

    for i in range(5):
        total = sum(kata_terbanyak)
        probabilitas = (kata_terbanyak[i]/total)*100
        prob.append(probabilitas)

    prob = [round(x) for x in prob]

    return bahasa, prob


if __name__ == '__main__':

    # text = '''
    # TWICE meraih ketenaran pada tahun 2016 dengan lagu "Cheer Up" yang menduduki peringkat 1 pada Gaon Digital Chart dan menjadi singel dengan penjualan terbaik di tahun yang sama. Lagu ini juga memenangkan gelar Song of the Year pada acara penghargaan musik utama seperti Melon Music Awards dan Mnet Asian Music Awards.[3][4][5] Lagu yang lain, "TT" dari album mini ketiga mereka Twicecoaster: Lane 1, menduduki puncak tangga lagu selama empat minggu berturut-turut. Album ini merupakan album grup wanita dengan penjualan terbaik untuk tahun 2016, dan terjual sebanyak 350,852 kopi pada akhir tahun.[6][7] Hanya dalam waktu 19 bulan setelah debut, TWICE telah menjual lebih dari 1.2 juta kopi, melalui empat album mini dan satu album spesial
    # '''
    text = '''
    TWICE meraih ketenaran pada tahun 2016 dengan lagu "Cheer Up" yang menduduki peringkat 1
    '''

    bahasa, prob = deteksi(text)

    for i in range(5):
        print("Bahasa : {}\n".format(bahasa[i]))
        print("Pobabilitas : {}%\n".format(round(prob[i])))
        print("="*50)
