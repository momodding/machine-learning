NER Detector
Named Entity Recognition

Requirements :
- Python 3
- Pandas
- Numpy
- Sklearn
- Falcon
- Gunicorn

Cara Penggunaan :
- Masuk ke python cli
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Training :
- isi url dengan localhost:8904/api/train dan ganti method menjadi POST
- pada tab body pilih raw -> application/json
- {"size":0.8, "engine":"randomforest"} atau {"size":0.8, "engine":"svm"}

Note:
- size adalah size training
- engine terdiri dari "engine 1" dan "engine 2"

Cara Testing API dengan postman:
- isi url dengan localhost:8904/api/predict dan ganti method menjadi POST
- pada tab body pilih raw -> application/json
- {"input":"Jessica", "engine":"randomforest"} atau {"input":"Jessica", "engine":"svm"}

Note:
- engine terdiri dari "randomforest" dan "svm"

Akurasi:
- Random Forest : 72%
- SVM : 72%