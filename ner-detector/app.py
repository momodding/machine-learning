import falcon
import json
# from core_multiNB import *
# from core_decisiontree import *
from core import *
from regex import *
from keras import backend as K
import falcon_jsonify


class HelloWorldResource:
    def on_get(self, req, resp, foo):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ("Hello " + foo +
                     ".  Python's Falcon thinks you are awesome!")


class DetekResource:
    def on_post(self, req, resp):
        """Handles POST requests"""
        inputan = req.json['input']
        engine = req.json['engine']
        if cek(inputan) <= 1:
            resp.status = falcon.HTTP_500
            resp.body = (json.dumps(
                {'status': "Error", 'message': "Kalimat kurang panjang"}))
        else:
            if engine == 'randomforest':
                rfm = load_model(engine)
                pred = predict(rfm, inputan)
                rgx = regex(inputan)
                result = [pred, rgx]
            else:
                engine = 'svm'
                svmm = load_model(engine)
                pred = predict(svmm, inputan)
                rgx = regex(inputan)
                result = [pred, rgx]
            K.clear_session()
            resp.status = falcon.HTTP_200
            resp.body = (json.dumps(
                {'status': "success", 'prediksi': result}))


class TrainResource:
    def on_post(self, req, resp):
        """Handles POST requests"""
        train_size = req.json['size']
        engine = req.json['engine']
        if engine == 'randomforest':
            accuracy = start_train_rf(train_size=train_size)
            print("Akurasi : ", accuracy)
        else:
            accuracy = start_train_svm(train_size=train_size)
            print("Akurasi : ", accuracy)
        K.clear_session()
        resp.status = falcon.HTTP_200
        resp.body = (json.dumps(
            {'status': "success", 'akurasi': accuracy}))


app = falcon.API(middleware=[falcon_jsonify.Middleware(help_messages=True)])
app.req_options.auto_parse_form_urlencoded = True

app.add_route('/hello/{foo}', HelloWorldResource())
app.add_route('/api/predict', DetekResource())
app.add_route('/api/train', TrainResource())
