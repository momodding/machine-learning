import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.cross_validation import cross_val_predict
from sklearn.metrics import classification_report
import nltk
from string import *
from sklearn.svm import SVC
from sklearn.externals import joblib
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.models import Model, Input
from keras.layers import LSTM, Embedding, Dense, TimeDistributed, Dropout, Bidirectional
from keras_contrib.layers import CRF
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from seqeval.metrics import precision_score, recall_score, f1_score, classification_report
from keras_contrib.layers import CRF
from keras.models import load_model


class SentenceGetter(object):

    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False

        def agg_func(s): return [(w, t) for w, t in zip(s["Word"].values.tolist(),
                                                        s["Tag"].values.tolist())]
        self.grouped = self.data.groupby("Sentence").apply(agg_func)
        self.sentences = [s for s in self.grouped]

    def get_next(self):
        try:
            s = self.grouped["{}".format(np.float(self.n_sent))]
            self.n_sent += 1
            return s
        except:
            return None


def set_config(gpu_memory=0.6, gpu_device="0"):
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = gpu_memory
    config.gpu_options.visible_device_list = gpu_device
    set_session(tf.Session(config=config))


def cek(text):
    """Cek banyak kata dalam teks"""
    tokens = nltk.wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]
    words = [''.join(c for c in s if c not in punctuation) for s in words]

    return len(words)


def load_data(filename):
    data = pd.read_csv(filename, sep="\t", names=['Word', 'Tag'])
    return data


def preprocess(data):
    words = [feature_map(w) for w in data["Word"].values.tolist()]
    tags = data["Tag"].values.tolist()
    return words, tags


def feature_map(word):
    '''Simple feature map.'''
    return np.array([word.istitle(), word.islower(), word.isupper(), len(word),
                     word.isdigit(),  word.isalpha()])


def train_random_forest(data_train, target_train, random, tree=20, method='entropy'):
    forest = RandomForestClassifier(
        n_estimators=tree, criterion=method, random_state=random)
    model = forest.fit(data_train, target_train)
    joblib.dump(model, 'model/randomforest.pkl')
    return model


def acc_random_forest(data_train, target_train, random, tree=20, method='entropy'):
    rfm = joblib.load('model/randomforest.pkl')
    acc = rfm.score(X=X_test, y=y_test)
    return acc


def start_train_rf(train_size=0.6):
    data = load_data('./ner/data_clean.txt')
    words = list(set(data["Word"].values))
    n_words = len(words)
    random = np.random.randint(0, n_words, size=1)

    words, tags = preprocess(data)
    X_train, X_test, y_train, y_test = train_test_split(
        words, tags, train_size=train_size, random_state=random[0])

    rf = train_random_forest(X_train, y_train, random=random[0])
    acc = rf.score(X=X_test, y=y_test)
    return acc


def train_svm(data_train, target_train, kernel='rbf'):
    svmc = SVC(kernel=kernel)
    model = svmc.fit(data_train, target_train)
    joblib.dump(model, 'model/svm.pkl')
    return model


def acc_svm(data_train, target_train, kernel='rbf'):
    svmm = joblib.load('model/svm.pkl')
    acc = svmm.score(X=X_test, y=y_test)
    return acc


def start_train_svm(train_size=0.6):
    data = load_data('./ner/data_clean.txt')
    words = list(set(data["Word"].values))
    n_words = len(words)
    random = np.random.randint(0, n_words, size=1)

    words, tags = preprocess(data)
    X_train, X_test, y_train, y_test = train_test_split(
        words, tags, train_size=train_size, random_state=random[0])

    svm = train_svm(X_train, y_train)
    acc = svm.score(X=X_test, y=y_test)
    return acc


def load_model(engine):
    if engine == 'randomforest':
        model = joblib.load('model/randomforest.pkl')
    else:
        model = joblib.load('model/svm.pkl')
    return model


def pred2label(pred):
    out = []
    for pred_i in pred:
        out_i = []
        for p in pred_i:
            p_i = np.argmax(p)
            out_i.append(idx2tag[p_i].replace("PAD", "O"))
        out.append(out_i)
    return out


def word_tag(data):
    words = list(set(data["Word"].values))
    words.append("ENDPAD")

    tags = list(set(data["Tag"].values))
    return words, tags


def start_train_lstm(train_size=0.6):
    data = load_data('./ner/train1234_clean.txt')
    words, tags = word_tag(data)
    n_words = len(words)

    random = np.random.randint(0, n_words, size=1)

    n_tags = len(tags)

    getter = SentenceGetter(data)
    sentences = getter.sentences

    max_len = 100
    word2idx = {w: i + 1 for i, w in enumerate(words)}
    tag2idx = {t: i for i, t in enumerate(tags)}

    X = [[word2idx[w[0]] for w in s] for s in sentences]
    X = pad_sequences(maxlen=max_len, sequences=X,
                      padding="post", value=n_words-1)

    y = [[tag2idx[w[1]] for w in s] for s in sentences]
    y = pad_sequences(maxlen=max_len, sequences=y,
                      padding="post", value=tag2idx["O"])

    y = [to_categorical(i, num_classes=n_tags) for i in y]

    X_tr, X_te, y_tr, y_te = train_test_split(
        X, y, train_size=train_size, random_state=random[0])

    input = Input(shape=(max_len,))
    model = Embedding(input_dim=n_words + 1, output_dim=20,
                      input_length=max_len, mask_zero=True)(input)  # 20-dim embedding
    model = Bidirectional(LSTM(units=50, return_sequences=True,
                               recurrent_dropout=0.2))(model)  # variational biLSTM
    # model = LSTM(units=100, return_sequences=True,
    #                            recurrent_dropout=0.2)(model)
    model = TimeDistributed(Dense(50, activation="relu"))(
        model)  # a dense layer as suggested by neuralNer
    crf = CRF(n_tags)  # CRF layer
    out = crf(model)  # output

    model = Model(input, out)
    model.compile(optimizer="rmsprop", loss=crf.loss_function,
                  metrics=[crf.accuracy])

    set_config()

    model.fit(X_tr, np.array(y_tr), batch_size=32, epochs=8,
              validation_split=0.2, verbose=1)

    model.save('model/bi-lstm.h5')

    idx2tag = {i: w for w, i in tag2idx.items()}

    pred_labels = pred2label(test_pred)
    test_labels = pred2label(y_te)

    acc = f1_score(test_labels, pred_labels)
    return acc


def create_custom_objects():
    instanceHolder = {"instance": None}

    class ClassWrapper(CRF):
        def __init__(self, *args, **kwargs):
            instanceHolder["instance"] = self
            super(ClassWrapper, self).__init__(*args, **kwargs)

    def loss(*args):
        method = getattr(instanceHolder["instance"], "loss_function")
        return method(*args)

    def accuracy(*args):
        method = getattr(instanceHolder["instance"], "accuracy")
        return method(*args)
    return {"ClassWrapper": ClassWrapper, "CRF": ClassWrapper, "loss": loss, "accuracy": accuracy}


def load_keras_model(path):
    model = load_model(path, create_custom_objects())
    return model


def predict_lstm(sentence):
    test_sentence = nltk.word_tokenize(sentence)
    data = load_data('./ner/train1234_clean.txt')
    words, tags = word_tag(data)
    word2idx = {w: i + 1 for i, w in enumerate(words)}
    x_test_sent = pad_sequences(sequences=[[word2idx.get(w, 0) for w in test_sentence]],
                                padding="post", value=0, maxlen=100)

    model = load_keras_model('model/bi-lstm.h5')

    p = model.predict(np.array([x_test_sent[0]]))
    p = np.argmax(p, axis=-1)
    event = []
    loc = []
    other = []
    org = []
    person = []
    qty = []
    time = []
    pred_sent = list(zip(test_sentence, p[0]))
    print(pred_sent)
    for line in range(len(pred_sent)):
        print(test_sentence[line], "->", tags[p[0][line]])
        if tags[p[0][line]] == 'PERSON':
            person.append(test_sentence[line])
        elif tags[p[0][line]] == 'ORGANIZATION':
            org.append(test_sentence[line])
        elif tags[p[0][line]] == 'LOCATION':
            loc.append(test_sentence[line])
        elif tags[p[0][line]] == 'EVENT':
            event.append(test_sentence[line])
        elif tags[p[0][line]] == 'QUANTITY':
            qty.append(test_sentence[line])
        elif tags[p[0][line]] == 'TIME':
            time.append(test_sentence[line])
        elif tags[p[0][line]] == 'O':
            other.append(test_sentence[line])
    final_result = {
        'person': person,
        'event': event,
        'location': loc,
        'organization': org,
        'quantity': qty,
        'time': time,
        'other': other
    }
    return final_result


def predict(model, sentence):
    test_sentence = nltk.word_tokenize(sentence)
    pred = [feature_map(s) for s in test_sentence]
    p = model.predict(pred)
    result = list(zip(test_sentence, p))
    event = []
    loc = []
    other = []
    org = []
    person = []
    qty = []
    time = []
    for i in range(len(result)):
        if result[i][1] == 'PERSON':
            person.append(result[i][0])
        elif result[i][1] == 'ORGANIZATION':
            org.append(result[i][0])
        elif result[i][1] == 'LOCATION':
            loc.append(result[i][0])
        elif result[i][1] == 'EVENT':
            event.append(result[i][0])
        elif result[i][1] == 'QUANTITY':
            qty.append(result[i][0])
        elif result[i][1] == 'TIME':
            time.append(result[i][0])
        elif result[i][1] == 'O':
            other.append(result[i][0])
    final_result = {
        'person': person,
        'event': event,
        'location': loc,
        'organization': org,
        'quantity': qty,
        'time': time,
        'other': other
    }
    return final_result


if __name__ == "__main__":
    data = load_data('./ner/data_clean.txt')

    # words = list(set(data["Word"].values))
    # n_words = len(words)
    # random = np.random.randint(0, n_words, size=1)

    # words, tags = preprocess(data)
    # X_train, X_test, y_train, y_test = train_test_split(
    #     words, tags, test_size=0.4, random_state=random[0])

    print("start train and save model\n")
    # rf = train_random_forest(X_train, y_train, random=random[0])
    # svm = train_svm(X_train, y_train)

    # print("calculate accuracy\n")
    # print("randomforest acc : ", start_train_rf())
    # print("svm acc : ", start_train_svm())

    print("load model\n")
    rfm = joblib.load('model/randomforest.pkl')
    svmm = joblib.load('model/svm.pkl')

    print("make prediction\n")
    test_sentence = 'Upaya Airlangga serius bertarung, tapi melihat gelagat tidak sehat, maka lebih baik mundur.  Itu pun jalan moderat," kata Arie. Dengan mundurnya Hidayat dan Airlangga, Aburizal berpotensi menjadi calon tunggal ketua umum yang akan dipilih dalam Munas IX.'
#     test_sentence = '''Wakil Ketua Umum Partai Demokrat Nurhayati Ali Assegaf mengatakan, faktor utama yang membuat Partai Demokrat bisa besar seperti saat ini ialah karena pengaruh dari Ketua Umum Partai Demokrat saat ini, Susilo Bambang Yudhoyono.
# Nurhayati lalu membandingkan Partai Demokrat dengan Partai Demokrasi Indonesia Perjuangan yang juga besar karena faktor ketokohan pemimpinnya.
# "Kalau di Indonesia misalnya, PDI-P lahir dan besar karena trahnya Soekarno.  Megawati akan tetap memegang trahnya karena Soekarno.  Sama halnya dengan Demokrat lahir dan besar karena SBY," ujar Nurhayati, di Kompleks Parlemen Senayan, Jakarta, Kamis (18/12/2014).
# Nurhayati mengatakan, hampir sebagian besar kader Demokrat, termasuk dirinya, masuk menjadi kader Demokrat karena faktor SBY'''
    rf_result = predict(rfm, test_sentence)
    sv_result = predict(svmm, test_sentence)
    lstm_result = predict_lstm(test_sentence)

    print(sv_result)
