import re

def regex(test_str):
    url = []
    mail = []
    phone = []
    aom = []
    religion = []

    find_url = r"((www\.[^\s]+)|(https?://[^\s]+))"
    find_mail = r"[\w\.-]+@[\w\.-]+"
    find_phone = r"((\+628)?\s*?(\d{3})\s*?(\d{3})\s*?(\d{4})|(\08)?\s*?(\d{3})\s*?(\d{3})\s*?(\d{3})\s*?(\d{3}))"
    find_aom = r"((\d+\.\d{1,3}\.\d{1,3}\.\d{1,3})|(\d+\.\d{1,3}\.\d{1,3})|(\d+\.\d{1,3}))"
    find_religion = r"\b(Islam|Kristen|Katolik|Buddha|Hindu)\b"

    m_url = re.finditer(find_url, test_str, re.MULTILINE |
                        re.IGNORECASE | re.VERBOSE | re.DOTALL | re.UNICODE)
    for i, match_url in enumerate(m_url):
        # print(match_url.group())
        url.append(match_url.group())

    m_phone = re.finditer(find_phone, test_str, re.MULTILINE |
                          re.IGNORECASE | re.VERBOSE | re.DOTALL | re.UNICODE)
    for i, match_phone in enumerate(m_phone):
        # print(match_phone.group())
        phone.append(match_phone.group())

    m_mail = re.finditer(find_mail, test_str, re.MULTILINE |
                         re.IGNORECASE | re.VERBOSE | re.DOTALL | re.UNICODE)
    for i, match_mail in enumerate(m_mail):
        # print(match_mail.group())
        mail.append(match_mail.group())

    m_aom = re.finditer(find_aom, test_str, re.MULTILINE |
                        re.IGNORECASE | re.VERBOSE | re.DOTALL | re.UNICODE)
    for i, match_aom in enumerate(m_aom):
        # print(match_aom.group())
        aom.append(match_aom.group())

    m_religion = re.finditer(find_religion, test_str, re.MULTILINE |
                             re.IGNORECASE | re.VERBOSE | re.DOTALL | re.UNICODE)
    for i, match_religion in enumerate(m_religion):
        # print(match_religion.group())
        religion.append(match_religion.group())

    result = {
        'url': url,
        'phone': phone,
        'mail': mail,
        'money': aom,
        'religion': religion
    }

    return result


if __name__ == "__main__":
    test_str = "kamu buka yutub https://www.youtube.com aja @teza_rabrin kan https://www.facebook.com email katolik kamu teza@rabrin.com 089611228978 harganya 10.000.000.000 islam nomerku 081242765243 duitku 10.000"
    hasil = regex(test_str)
    print(hasil)
