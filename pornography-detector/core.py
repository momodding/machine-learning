import pandas as pd
import numpy as np
from sklearn.utils import shuffle
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.externals import joblib
import tensorflow as tf
from keras.models import Model, Sequential
from keras.layers import Input, Activation, Dense
from keras.optimizers import SGD
from keras.utils.np_utils import to_categorical
from keras.models import model_from_json
import matplotlib.pylab as plt


def load_data(filename):
    df = pd.read_csv(filename)
    X = np.array(df.drop(['KELAS'], 1))
    Y = np.array(df['KELAS'])

    return X, Y


def split_data(x, y, train_split=0.8):
    X, Y = shuffle(x, y)
    X_train, X_test = X[:int(train_split * len(X))
                        ], X[int(train_split * len(X)):]
    Y_train, Y_test = Y[:int(train_split * len(Y))
                        ], Y[int(train_split * len(Y)):]

    return X_train, X_test, Y_train, Y_test


def svm_train(data_train, target_train):
    svclassifier = SVC(kernel='rbf')
    svm = svclassifier.fit(data_train, target_train)
    joblib.dump(svm, './model/svm.pkl')


def nn_train(X_train, X_test, Y_train, Y_test, epochs=300, batch_size=64, learning_rate=0.001):
    Y_train = to_categorical(Y_train, num_classes=None)
    Y_test = to_categorical(Y_test, num_classes=None)

    # create model
    nn = Sequential()
    nn.add(Dense(15, input_dim=13, activation='relu'))
    nn.add(Dense(8, activation='relu'))
    nn.add(Dense(4, activation='softmax'))
    # Compile model
    sgd = SGD(lr=learning_rate)
    nn.compile(loss='categorical_crossentropy',
               optimizer=sgd, metrics=['accuracy'])
    # Fit the model
    nn.fit(X_train, Y_train, epochs=epochs, batch_size=batch_size, verbose=2)

    scores = nn.evaluate(X_test, Y_test)
    print("\n%s: %.2f%%" % (nn.metrics_names[1], scores[1]*100))

    # serialize model to JSON
    model_json = nn.to_json()
    with open("./model/model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    nn.save_weights("./model/model.h5")
    print("Saved model to disk")


if __name__ == "__main__":
    file = 'glcm-data.txt'
    X, Y = load_data(file)
    X_train, X_test, Y_train, Y_test = split_data(X, Y)
    svm_train(X_train, Y_train)
    nn_train(X_train, X_test, Y_train, Y_test)

    svm_model = joblib.load('./model/svm.pkl')

    print(metrics.classification_report(Y_test, svm_model.predict(X_test)))
    print("Accuracy:", metrics.accuracy_score(
        Y_test, svm_model.predict(X_test)))

    # load json and create model
    # json_file = open('model.json', 'r')
    # nn_model_json = json_file.read()
    # json_file.close()
    # nn_model = model_from_json(nn_model_json)
    # # load weights into new model
    # nn_model.load_weights("model.h5")
    # sgd = SGD(lr=0.001)
    # nn_model.compile(loss='categorical_crossentropy',
    #                  optimizer=sgd, metrics=['accuracy'])
