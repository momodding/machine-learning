Semantic Analysis
Detecting similiarity from two sentence or paragraph

Requirements :
- Python 3
- NLTK
- NLTK stopword, punkt, wordnet
- Sastrawi
- Sklearn
- Falcon
- Gunicorn

Cara Penggunaan :
- Masuk ke python cli
- impot nltk
  nltk.download('stopwords')
  nltk.download('punkt')
  nltk.download('wordnet')
  exit()
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Akses API dengan postman:
- isi url dengan localhost:8881/api dan ganti method menjadi POST
- pada tab body pilih x-www-form-urlencoded
- pada kolom key isi inputan1, inputan2, engine, stopword
- pada kolom value isi inputan1 dan inputan2 dengan teks
- pada kolom engine isi dengan cosine atau wordnet
- pada kolom stopword isi dengan stopword bahasa yang digunakan