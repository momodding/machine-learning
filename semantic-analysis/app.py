import falcon
import json
from cosine_core import *
from wordnet_core import *
from checker import *
import falcon_jsonify


class HelloWorldResource:
    def on_get(self, req, resp, foo):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ("Hello " + foo +
                     ".  Python's Falcon thinks you are awesome!")


class DetekResource:
    def on_post(self, req, resp):
        """Handles POST requests"""
        inputan1 = req.json["inputan1"]
        inputan2 = req.json["inputan2"]
        engine = req.json["engine"]
        stopword = req.json["stopword"]
        if cek_text(inputan1) <= 5 or cek_text(inputan2) <= 5:
            resp.status = falcon.HTTP_500
            resp.body = (json.dumps(
                {'status': "Error", 'emssage': "Kalimat kurang panjang"}))
        elif cek_text(inputan1) > 5 and cek_text(inputan2) > 5 and engine == "cosine":
            txt1 = cosine_preprocessing(inputan1)
            txt2 = cosine_preprocessing(inputan2)
            hasil = predict(txt1, txt2)
            resp.status = falcon.HTTP_201
            resp.body = (json.dumps(
                {'status': "success", 'persentase': hasil}))
        elif cek_text(inputan1) > 5 and cek_text(inputan2) > 5 and engine == "wordnet":
            text1 = wordnet_preprocessing(inputan1, stopword=stopword)
            text2 = wordnet_preprocessing(inputan2, stopword=stopword)
            hasil = calculate(text1, text2)
            resp.status = falcon.HTTP_201
            resp.body = (json.dumps(
                {'status': "success", 'persentase': hasil}))


app = falcon.API(middleware=[falcon_jsonify.Middleware(help_messages=True)])
app.req_options.auto_parse_form_urlencoded = True

app.add_route('/hello/{foo}', HelloWorldResource())
app.add_route('/api', DetekResource())
