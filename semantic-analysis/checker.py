from string import *
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords

def cek_text(text):
    """Cek banyak kata dalam teks"""
    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]
    words = [''.join(c for c in s if c not in punctuation) for s in words]

    return len(words)