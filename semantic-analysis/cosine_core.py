from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import re
from string import *
import numpy as np
import nltk
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords
from nltk.tokenize.treebank import TreebankWordDetokenizer as Detokenize


def cosine_preprocessing(data):
    tokens = wordpunct_tokenize(data)
    words = [word.lower() for word in tokens]
    words = [''.join(c for c in s if c not in punctuation) for s in words]

    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    stem = [stemmer.stem(item) for item in words]

    detokenizer = Detokenize()
    sentence = detokenizer.detokenize(stem)

    return sentence


def predict(text1, text2):
    vectorizer = TfidfVectorizer()
    tfidf = vectorizer.fit_transform([text1, text2])
    result = ((tfidf * tfidf.T).A)[0, 1]

    return round(result, 2)*100


def main():
    text1 = 'Permasalahan pasca panen pada buah belimbing yang diproduksi secara skala besar atau industri adalah penyortiran. Saat ini buah belimbing diklasifikasikan berdasarkan analisa warna kulit buah secara visual mata manusia. Metode ini tidak efektif dan efisien. Penelitian ini bertujuan untuk mengklasifikasikan buah belimbing berdasarkan tingkat kemanisan menggunakan teknik pemrosesan citra. Ekstraksi fitur yang digunakan adalah nilai Red, Green, dan Blue (RGB) untuk mendapatkan ciri pada citra warna. Kemudian hasil ekstraksi fitur tersebut digunakan untuk mengklasifikasikan buah belimbing dengan metode Naïve Bayes. Data citra belimbing yang digunakan berjumlah 120 yang terdiri dari data latih berjumlah 90 dan data uji berjumlah 30. Hasil klasifikasi menunjukkan akurasi menggunakan ekstraksi ciri RGB sebesar 80%. Penggunaan RGB sebagai ciri warna belum bisa digunakan sepenuhnya sebagai fitur dari citra buah belimbing.'
    text2 = 'Melalui Undang-Undang Aparatur Sipil Negara, Pemerintah melakukan upaya mengurangi nepotisme dengan menciptakan sistem kompetisi yang terbuka di antara PNS dalam proses pengisian jabatan. Badan Kepegawaian Daerah (BKD) Tarakan telah memiliki basis data pegawai dan sistem pendukung keputusan yang dapat menggabungkan basis data yang tersedia dengan model penilaian untuk mendapatkan profil calon yang cocok dengan profil jabatan yang lowong diperlukan untuk menunjang kinerja yang lebih objektif.\
    Penerapan metode profile matching dalam sistem pengambilan keputusan ini diharapkan dapat membantu proses rekomendasi pemilihan kandidat pejabat struktural di lingkungan Pemerintahan Kota Tarakan sesuai dengan kemampuan bidang yang dibutuhkan dalam suatu jabatan.\
    Dari hasil penelitian diperoleh kesimpulan bahwa perubahan nilai profil kandidat, dan jumlah subkriteria yang digunakan untuk kriteria jabatan, dapat mempengaruhi kedekatan kandidat dengan jabatan yang tersedia dan penggunaan metode profile matching untuk kasus yang mengganggap bahwa nilai tertinggi adalah nilai terbaik mengharuskan nilai ideal yang digunakan merupakan nilai maksimum agar tidak terjadi ekspektasi yang melebihi nilai ideal.'
    txt1 = cosine_preprocessing(text1)
    txt2 = cosine_preprocessing(text2)
    hasil = predict(txt1, txt2)

    # print ("{}%".format(round(hasil, 2)*100))
    print(hasil, "%")
    return hasil


if __name__ == '__main__':
    main()
