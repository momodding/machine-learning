from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords, wordnet
from nltk.stem import WordNetLemmatizer
from itertools import product
import numpy, nltk

def wordnet_preprocessing(text, stopword):
    stop_words = set(stopwords.words(stopword))
    filtered_sentence = []
    lemm_sentence = []
    lemmatizer = WordNetLemmatizer()

    for words in word_tokenize(text):
        if words not in stop_words:
            if words.isalnum():
                filtered_sentence.append(words)

    for i in filtered_sentence:
        lemm_sentence.append(lemmatizer.lemmatize(i))

    return lemm_sentence

def calculate(text1, text2):
    final = []

    for i in text1:
        simi = []
        for x in text2:
            sims = []
            synsets_i = wordnet.synsets(i)
            synsets_x = wordnet.synsets(x)
            for syn_i, syn_x in product(synsets_i, synsets_x):
                list_similiarity = wordnet.wup_similarity(syn_i, syn_x)
                if list_similiarity != None:
                    sims.append(list_similiarity)

            if sims != []:
                max_sim = max(sims)
                simi.append(max_sim)

        if simi != []:
            max_final = max(simi)
            final.append(max_final)

    similarity_index = numpy.mean(final)
    similarity_index = round(similarity_index, 2)*100
    return similarity_index

def main():
    str1 = "The boy is fetching water from the well."
    str2 = "The lion is running in the forest."
    text1 = wordnet_preprocessing(str1, stopword="english")
    text2 = wordnet_preprocessing(str2, stopword="english")
    print (calculate(text1, text2),"%")

if __name__ == '__main__':
    main()