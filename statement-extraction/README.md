Statement Extractor
Detecting statement from paragraph or article

Requirements :
- Python 3
- NLTK
- Spacy
- Falcon
- Gunicorn

Cara Penggunaan :
- Masuk ke cli/terminal
- python -m spacy download xx
- kemudian ketikkan sudo chmod +x start.sh
- ketikkan ./start.sh

Cara Akses API dengan postman:
- isi url dengan localhost:8906/api dan ganti method menjadi POST
- pada tab body pilih raw -> application/json
- {"inputan":"Jokowi mengatakan bahwa besok akan datang ke acara tersebut."}