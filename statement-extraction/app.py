import falcon
import json
from core import *
import falcon_jsonify


class HelloWorldResource:
    def on_get(self, req, resp, foo):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = ("Hello " + foo +
                     ".  Python's Falcon thinks you are awesome!")


class DetekResource:
    def on_post(self, req, resp):
        """Handles POST requests"""
        inputan = req.json['inputan']
        if cek(inputan) <= 5:
            resp.status = falcon.HTTP_500
            resp.body = (json.dumps(
                {'status': "Error", 'result': "Kalimat kurang panjang"}))
        else:
            result = regex(inputan)
        resp.status = falcon.HTTP_200
        resp.body = (json.dumps(
            {'status': "success", 'result': result}))


app = falcon.API(middleware=[falcon_jsonify.Middleware(help_messages=True)])
app.req_options.auto_parse_form_urlencoded = True

app.add_route('/hello/{foo}', HelloWorldResource())
app.add_route('/api', DetekResource())
