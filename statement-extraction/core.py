import re
import spacy
import nltk
from string import *


def cek(text):
    """Cek banyak kata dalam teks"""
    tokens = nltk.wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]
    words = [''.join(c for c in s if c not in punctuation) for s in words]

    return len(words)


def ner(example, show=False):
    parser = spacy.load('xx')
    if show:
        print(example)
    parsedEx = parser(example)

    # print("-------------- entities only ---------------")
    # if you just want the entities and nothing else, you can do access the parsed examples "ents" property like this:
    ents = list(parsedEx.ents)
    tags = {}
    for entity in ents:
        #print(entity.label, entity.label_, ' '.join(t.orth_ for t in entity))
        term = ' '.join(t.orth_ for t in entity)
        if ' '.join(term) not in tags:
            tags[term] = [(entity.label_)]
        else:
            tags[term].append((entity.label_))
    return tags


def get_tags(sentence):
    result = ner(sentence)
    person = []
    for i, val in result.items():
        #     print(val[0])
        if val[0] == 'PER':
            person.append(i)
    return person


def regex(test_str):
    direct = []
    indirect = []

    person = get_tags(test_str)

    r_direct = r"""
	\"[^\"]*\"
	"""

    m_direct = re.finditer(r_direct, test_str, re.MULTILINE |
                           re.IGNORECASE | re.VERBOSE | re.DOTALL)
    if m_direct:
        for word, match in enumerate(m_direct):
            # print(match.group())
            direct.append(match.group())

    r_indirect = r"(mengatakan(.*?)*\.)|(berkata*(.*?)*\.)"

    m_indirect = re.finditer(r_indirect, test_str, re.MULTILINE)
    if m_indirect:
        for word, match in enumerate(m_indirect):
            # print(match.group())
            indirect.append(match.group())

    result = {
        'influencer': person,
        'direct_statement': direct,
        'indirect_statement': indirect
    }

    return result


if __name__ == "__main__":
    q1 = '''"Saya tidak pernah ke sana", kata Budi.'''
    q2 = '''Jokowi mengatakan bahwa besok akan datang ke acara tersebut'''
    # hasil = regex(test_str)
    # print(hasil)
    q3 = '''"Saya tidak pernah ke sana", kata Budi. Namun, Jokowi mengatakan bahwa besok akan datang ke acara tersebut.'''

    res = regex(q3)
    print(res)
