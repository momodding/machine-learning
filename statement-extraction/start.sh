#!/bin/sh

# screen gunicorn app:app -b localhost:8881
if lsof -n -i4TCP:8881 | grep LISTEN > /dev/null; then
    kill -9 $(lsof -n -i4TCP:8881 | grep LISTEN | awk '{ print $2 }' | xargs kill)
    echo "process killed"
    screen gunicorn app:app -b localhost:8881
else
    screen gunicorn app:app -b localhost:8881
fi
